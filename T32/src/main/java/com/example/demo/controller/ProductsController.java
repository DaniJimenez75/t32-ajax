package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.example.demo.dto.Products;
import com.example.demo.service.ProductsServiceImpl;


@RestController
@CrossOrigin(origins="*",methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.DELETE})
public class ProductsController {
	@Autowired
	ProductsServiceImpl productsServiceImpl;

	@GetMapping("/products")
	public List<Products> listarProducts(){
		return productsServiceImpl.listarProducts();
	}
	
	@PostMapping("/products")
	public Products salvarDepartamento(@RequestBody Products products) {
		
		return productsServiceImpl.guardarProduct(products);
	}
	
	@GetMapping("/products/{id}")
	public Products productsXid(@PathVariable(name="id") int id) {
		
		Products product_xid= new Products();
		
		product_xid=productsServiceImpl.productXid(id);
		
		System.out.println("Product XId: "+product_xid);
		
		return product_xid;
	}
	

	@DeleteMapping("/products/{id}")
	public void eliminarProduct(@PathVariable(name="id")int id) {
		productsServiceImpl.eliminarProduct(id);
	}

}
