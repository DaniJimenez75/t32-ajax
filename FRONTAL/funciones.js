addEventListener('load', inicializarEventos, false);

function inicializarEventos() {
    document.getElementById("B1").addEventListener('click', presionB1, false);
    document.getElementById("B2").addEventListener('click', presionB2, false);
    document.getElementById("B3").addEventListener('click', presionB3, false);
    document.getElementById("B4").addEventListener('click', presionB4, false);
}

function presionB1() {
    getProducts();
}
function presionB2() {
    getProductByID(3);
}
function presionB3() {
    postProduct('PT69', 'ESTO ES UNA PRUEBA');
}
function presionB4() {
    deleteProduct(3);
}

function getProducts() {
    $.ajax({
        url: "http://localhost:8080/products",
        method: 'GET',
        dataType: 'json',
        headers: {
            'Accept': 'application/json',
            'Authoritation': ''
        },
        contentType: 'application/x-www-form-urlencoded',

        success: function (data) {
            console.log(JSON.stringify(data));
        },
        error: function (error) {
            console.log(error);
        }
    });
}

function getProductByID(id) {
    $.ajax({
        url: "http://localhost:8080/products/" + id,
        method: 'GET',
        dataType: 'json',
        headers: {
            'Accept': 'application/json',
            'Authoritation': ''
        },
        contentType: 'application/x-www-form-urlencoded',

        success: function (data) {
            console.log(JSON.stringify(data));
        },
        error: function (error) {
            console.log(error);
        }
    });
}

function postProduct(n, t) {
    $.ajax({
        url: "http://localhost:8080/products",
        method: 'POST',
        dataType: 'json',
        headers: {
            'Accept': 'application/json',
            'Authoritation': ''
        },
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({ name: n, detail: t }),
        success: function (data, textStatus, XHR) {
            alert('SUBIDO CORRECTAMENTE')
        },
        error: function (error) {
            console.log(error);
        }
    });
}

function deleteProduct(id) {
    $.ajax({
        url: "http://localhost:8080/products/" + id,
        method: 'DELETE',
        dataType: 'json',
        headers: {
            'Accept': 'application/json',
            'Authoritation': ''
        },
        contentType: 'application/x-www-form-urlencoded',

        success: function (data) {
            alert("ELIMINADO CORRECTAMENTE");
        },
        error: function (error) {
            console.log(error);
        }
    });
}